/// Or via https://twitter.com/dns2utf8
/// Source Code https://md.coredump.ch/mvbEZQ0xQyKUw6H3n8pYfQ

extern crate image;
extern crate threadpool;
extern crate walkdir;

use std::path::{self, Path, PathBuf};
use std::ffi::OsStr;

fn main() -> std::io::Result<()> {
    let source_images = std::env::var("SOURCE_DIR")
        .unwrap_or("source_images".to_string());
    println!("source_images: {:?} override with SOURCE_DIR", source_images);

    let target_images = std::env::var("TARGET_DIR")
        .unwrap_or("compressed_images".to_string());
    println!("target_images: {:?} override with TARGET_DIR", target_images);

    let target_images = PathBuf::from(&target_images);

    let pool = threadpool::Builder::new().build();

    for entry in walkdir::WalkDir::new(source_images.clone()) {
        let path = entry?.into_path();
        let source_images = source_images.clone();
        let target_images = target_images.clone();

        pool.execute(move || {
            let target = path.strip_prefix(source_images).expect("unable to do prefix work");
            let target = target_images.join(target);

            resize(&path, &target);
        });
    }

    pool.join();

    Ok( () )
}

fn resize(source: &Path, target: &Path) -> std::io::Result<()> {
    if source.is_file() == false {
        return Ok( () )
    }
    match image::open(source) {
        Ok(image) => {
            // crate the containing folder
            std::fs::create_dir_all(target.parent().expect("unable to find parent dir"))?;

            // resize
            // TODO add dynamic size selection
            let thumbnail = image.thumbnail(128, 128);
            thumbnail.save(target)?;
        }
        Err(err) => {
            println!("  (EE) {:?}", err);
        }
    };

    Ok( () )
}
